#include "locationHandler.h"

LocationHandler::LocationHandler(QObject *parent) : QObject(parent)
{
    source = QGeoPositionInfoSource::createDefaultSource(this);
   if (source) {
       connect(source, SIGNAL(positionUpdated(QGeoPositionInfo)),
                        this, SLOT(positionUpdated(QGeoPositionInfo)));
       source->startUpdates();
       source->setUpdateInterval(1000);
   }
}

void LocationHandler::positionUpdated(const QGeoPositionInfo &info)
{
      if(info.isValid())
      {
          currentLocation = info.coordinate();
//          qDebug() << "Current Latitude : " << currentLocation.latitude();
//          qDebug() << "Current Longitude : " << currentLocation.longitude();
          //updateDeviceCordinate(currentLocation);
          //emit newPositionData(info);
      }
}
