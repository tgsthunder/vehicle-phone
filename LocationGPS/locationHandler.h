#ifndef LOCATIONHANDLER_H
#define LOCATIONHANDLER_H

#include <QObject>
#include <QtPositioning/QGeoPositionInfo>
#include <QtPositioning/QGeoPositionInfoSource>
#include <QDebug>

class LocationHandler : public QObject
{
    Q_OBJECT
    public :
    explicit LocationHandler(QObject *parent = 0);
    QGeoCoordinate getCurrentLocation();
    QGeoPositionInfo m_currentData;
    private :
    QGeoCoordinate currentLocation;
    QGeoPositionInfoSource *source;
    signals :
    void newPositionData();
    public slots :
    void positionUpdated(const QGeoPositionInfo &info);
};
#endif // LOCATIONHANDLER_H
