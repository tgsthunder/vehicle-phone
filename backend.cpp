#include "backend.h"
#include <QtNetwork>

BackEnd::BackEnd(QObject *parent) :
    QObject(parent)
  ,m_tcpSocket(new QTcpSocket(this))
{
    m_settings=new QSettings("TGS","VehiclePhone");
    m_serverIP=m_settings->value("pit/serverIP").toString();
    if (m_serverIP=="")m_serverIP="10.147.20.148";
    m_pitPhoneNumber=m_settings->value("pit/phone").toString();
    m_port=myStrToI(m_settings->value("pit/serverPort","5100").toString());
    m_GeoSource = QGeoPositionInfoSource::createDefaultSource(this);
   if (m_GeoSource) {
       connect(m_GeoSource, SIGNAL(positionUpdated(QGeoPositionInfo)),
                        this, SLOT(positionUpdated(QGeoPositionInfo)));
       m_GeoSource->startUpdates();
       m_GeoSource->setUpdateInterval(1000);  //1000ms between GPS samples
   }
   m_currentData.cadence=0;
   m_currentData.current=0;
   m_currentData.latitude=0;
   m_currentData.longitude=0;
   m_currentData.setpoint=0;
   m_currentData.speed=0;
   m_currentData.temperature=1;
   m_currentData.throttle=0;
   m_currentData.voice=0;
   //pit comms setup
   m_udpSendSocket = new QUdpSocket(this);
   m_in.setDevice(m_tcpSocket);
   m_in.setVersion(QDataStream::Qt_4_0);
   QNetworkConfigurationManager manager;
   if(manager.capabilities() & QNetworkConfigurationManager::NetworkSessionRequired)
   {
       //get saved network configuration
       QSettings settings(QSettings::UserScope, QLatin1String("QtProject"));
       settings.beginGroup(QLatin1String("QtNetwork"));
       const QString id=settings.value(QLatin1String("DefaultNetworkConfiguration")).toString();
       settings.endGroup();
       //if the saved network configuration is not currently discovered, use the system default
       QNetworkConfiguration config=manager.configurationFromIdentifier(id);
       if ((config.state()&QNetworkConfiguration::Discovered)!=QNetworkConfiguration::Discovered)
       {
           config=manager.defaultConfiguration();
       }
       m_networkSession=new QNetworkSession(config, this);
       connect(m_networkSession, &QNetworkSession::opened, this, &BackEnd::TcpSessionOpened);
       m_networkSession->open();
   }
   //TcpConnect();
   /* Signal and Slots */
   /* Search Button */
   connect(this, SIGNAL(btScan()),&m_bleConnection, SLOT(startScan()));
   /* Connect Button */
   connect(this, SIGNAL(connectToDevice(int)),&m_bleConnection,SLOT(startConnect(int)));
   /* Send Data Button */
//   connect(m_pBsend,SIGNAL(clicked()),this, SLOT(sendData()));
   /* Bleutooth States */
   connect(&m_bleConnection, SIGNAL(changedState(bluetoothleUART::bluetoothleState)),this,SLOT(changedState(bluetoothleUART::bluetoothleState)));
   /* Handle phone calls to pit */
   connect(this,SIGNAL(togglePhone()),this,SLOT(handlePitCall()));
   /*TCP Sockets*/
   connect(m_tcpSocket, &QIODevice::readyRead, this, &BackEnd::TcpRead);
   connect(m_tcpSocket, QOverload<QAbstractSocket::SocketError>::of(&QAbstractSocket::error),this,&BackEnd::TcpError);


   QString folder=QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation);
   qWarning("folder=%s",qPrintable(folder));
   QDateTime temp= QDateTime::currentDateTime();
   QString filename = "/VehicleLog"+QDateTime::currentDateTime().toString("ddMMyy_hh-mm-ss")+".txt";
   m_file=new QFile(folder+filename);
   if (!m_file->exists()) {
       // create the folder, if necessary
       m_dir=new QDir(folder);
       if (!m_dir->exists()) {
           qWarning("creating new folder");
           m_dir->mkpath(".");
       }
       qWarning("creating new file");
       m_file->open(QIODevice::WriteOnly);
           m_file->write("date,time,latitude,longitude,cadence,speed,setpoint,throttle,current,temperature,voice\n");
   }
   keepScreenOn(true);
}

BackEnd::~BackEnd()
{
    m_file->close();
    keepScreenOn(false);
}
QString BackEnd::userName()
{
    return m_userName;
}

QStringList BackEnd::BTDeviceList()
{
    return m_BTDeviceList;   //.filter("TGS");
}

void BackEnd::setUserName(const QString &userName)
{
    if (userName == m_userName)
        return;
    m_userName = userName;
    emit userNameChanged();
}
int BackEnd::btIndex()
{
    return m_btIndex;
}
void BackEnd::setbtIndex(int val)
{
    m_btIndex=val;
    qWarning() << "m_btIndex: " << m_btIndex;
}
void BackEnd::ScanButtonClick(){
    emit btScan();
}
void BackEnd::ConnectButtonClick(){
    //emit connectToDevice(m_btIndex);
    qWarning() << "Attempting BT connection to : " << m_btIndex;
    m_bleConnection.startConnect(m_btIndex);
//   m_networkCheck=new QTimer(this);
//   connect (m_networkCheck,SIGNAL(timeout()),this,SLOT(NetworkCheck()));
//   qDebug() << "network check timer activate...";
//   m_networkCheck->start(10000);  //check the network status every 10 seconds
}
QString BackEnd::getServerIP()
{
    return m_serverIP;
}
void BackEnd::setServerIP(QString IP)
{
    m_serverIP=IP;
    m_settings->setValue("pit/serverIP",m_serverIP);
    m_tcpSocket->disconnect();
    QString hostIP=m_serverIP;
    m_tcpSocket->connectToHost(hostIP,m_port);
}
QString BackEnd::getServerPort()
{
    return QString::number(m_port);
}
void BackEnd::setServerPort(QString port)
{
    int temp=myStrToI(port);
    if (!temp) return;  //if conversion to int failed, return without changing port
    m_port=temp;
    m_settings->setValue("pit/serverPort",m_port);
     m_tcpSocket->disconnect();
     QString hostIP=m_serverIP;
     m_tcpSocket->connectToHost(hostIP,m_port);
}
int BackEnd::myStrToI(QString val)
{
    QRegularExpression re("(?<por>\\d+)");   //one or more digits
    QRegularExpressionMatch match1 = re.match(val);
    if (match1.hasMatch())
    {
        return match1.captured("por").toInt();
    }
    return 0;
}
QString BackEnd::getPitNumber()
{
    return m_pitPhoneNumber;
}
void BackEnd::setPitNumber(QString number)
{
    m_pitPhoneNumber=number;
    m_settings->setValue("pit/phone",m_pitPhoneNumber);
}
void BackEnd::positionUpdated(const QGeoPositionInfo &info)
{
      if(info.isValid())
      {
          if(m_attempt_reconnect ){
              writeToFile();  //we're disconnected so updates won't be sent. Send GPS data as it comes in so that we can follow vehicle anyway
              QString status="Device Disconnected... attempting reconnection...";
              status.append(QString::number(10-m_reconnect_count));
              changeStatus(status);
              if(m_reconnect_count++>9){ //we've been disconnected for 10 seconds or more... attempt to reconnect every 10 seconds
                  m_reconnect_count=0;
                  m_bleConnection.startConnect(m_btIndex);
              }
          }
          m_currentLocation = info.coordinate();
          m_GeoInfo=info;
          m_currentData.latitude=m_currentLocation.latitude();
          m_currentData.longitude=m_currentLocation.longitude();
          if (!m_bluetooth_initialised)
          {//bluetooth hasn't been initialised yet... still want positional data though...
              m_currentData.speed=info.GroundSpeed;
              m_currentData.cadence=0;
              QString status="bluetooth not initialised...";
              m_displayCounter=(m_displayCounter+1) % 10;
              status.append(QString::number(m_displayCounter));
              status.append(QString::number(m_tcpSocket->isValid()));
              changeStatus(status);
              writeToFile();  //we're disconnected so updates won't be sent. Send GPS data as it comes in so that we can follow vehicle anyway
              emit mainDataChanged();
          }
          emit locationChanged();
      }
}
QGeoCoordinate BackEnd::getCurrentLocation()
{
    return m_currentLocation;
}
QString BackEnd::getStatus()
{
    return m_status;
}
void BackEnd::changeStatus(QString newStatus)
{
    if (newStatus == m_status)
        return;
    m_status = newStatus;
    emit statusChanged();
}
void BackEnd::changedState(bluetoothleUART::bluetoothleState state){
    qDebug() << state;
    switch(state){
        case bluetoothleUART::Scanning:
        {
            changeStatus("Searching for low energy devices...");
            break;
        }
        case bluetoothleUART::ScanFinished:
        {
            m_bleConnection.getDeviceList(m_BTDeviceList);
            if(!m_BTDeviceList.empty()){
                for (int i = 0; i < m_BTDeviceList.size(); i++)
                {
                   //*********m_cbDevicesFound.addItem(m_BTDeviceList.at(i));
                }
                /* Initialise Slot startConnect(int) -> button press m_pBConnect */
//                connect(this, SIGNAL(connectToDevice(int)),&m_bleConnection,SLOT(startConnect(int)));
                changeStatus("Please select BLE device");
                m_btIndex=0;
                emit btDeviceListChanged();
            }
            else
            {
                changeStatus("No Low Energy devices found");
            }
            break;
        }
        case bluetoothleUART::Connecting:
        {
            changeStatus("Connecting to device...");
            break;
        }
        case bluetoothleUART::Connected:
        {
            m_attempt_reconnect=false;
            changeStatus("Device connected. Looking for service...");
            break;
        }
        case bluetoothleUART::ServiceFound:
        {
            changeStatus("Service found");
            break;
        }
        case bluetoothleUART::AcquireData:
        {
            /* Initialise Slot DataHandler(QString) - gets new data */
            connect(&m_bleConnection, SIGNAL(newData(QString)), this, SLOT(DataHandler(QString)));
            changeStatus("Aquire data");
            m_bluetooth_initialised=true;
            break;
        }
        case bluetoothleUART::Disconneted:
        {
            m_attempt_reconnect=true;
            changeStatus("Device Disconnected... attempting reconnection");
            break;
        }
        default:
            //nothing for now
        break;
    }
}
void BackEnd::DataHandler(const QString &s)
{
    m_BlueBuffer.append(s);
    bool a_match;
    /* We get a string "TempNNNNHumNNNNXYZ" (N: Number from 1 to 9)  */
    /* We get a string CadNNNSpeNNSetNNNThrNNNCurNNNTemNNN(N: Number from 1 to 9)  */
    //QRegularExpression re ("Temp(?<temp>-?\\d+)Hum(?<hum>\\d+)XYZ");
    do
    {
        a_match=false;
        QRegularExpression re1("Cad(?<cad>-?\\d+)Spe(?<spe>\\d+)Set(?<set>\\d+)Thr(?<thr>\\d+)Cur(?<cur>\\d+)Tem(?<tem>\\d+)Voi(?<voi>\\d+)XYZ");
        QRegularExpressionMatch match1 = re1.match(m_BlueBuffer);
        if (match1.hasMatch())
        {
            a_match=true;
            int startOffset = match1.capturedStart(1);
            QString captured= match1.captured(0);
            //int diffOffset = endOffset-startOffset-1;
            m_BlueBuffer.replace(0,startOffset+captured.length(),""); //strip out the matched data from the buffer
            QString matchedTempValue = match1.captured("cad");
            m_currentData.cadence=matchedTempValue.toInt();
            matchedTempValue = match1.captured("spe");
            m_currentData.speed=matchedTempValue.toInt();
            matchedTempValue = match1.captured("set");
            m_currentData.setpoint=matchedTempValue.toInt();
            matchedTempValue = match1.captured("thr");
            m_currentData.throttle=matchedTempValue.toInt();
            matchedTempValue = match1.captured("cur");
            m_currentData.current=matchedTempValue.toInt();
            matchedTempValue = match1.captured("tem");
            m_currentData.temperature=matchedTempValue.toInt();
            matchedTempValue = match1.captured("voi");
            m_currentData.voice=matchedTempValue.toInt();
            emit mainDataChanged();
            writeToFile();
//            if (m_currentData.voice){
//                emit togglePhone();  //call or hang up...
//            }
            //CDG: also... if we have a match, then this should be sent on to the pit over VPN...
        }
    }while(a_match);
}
void BackEnd::writeToFile()
{
    QString buff=QDateTime::currentDateTime().toString("dd/MM/yyyy,hh:mm:ss:zzz");
    buff.append(QString(",%1,%2,%3,%4,%5,%6,%7,%8\n")
                .arg(m_currentData.latitude,0,'f',15)
                .arg(m_currentData.longitude,0,'f',15)
                .arg(m_currentData.cadence)
                .arg(m_currentData.speed)
                .arg(m_currentData.setpoint)
                .arg(m_currentData.throttle)
                .arg(m_currentData.current)
                .arg(m_currentData.temperature)
                );
    const char* p =buff.toStdString().c_str();
//    m_file->write(qPrintable(buff));
    m_file->write(p);
    //broadcastData();
    TcpSend();
//    buff.prepend("Vehicle1:");
//    QByteArray udpBuff = buff.toUtf8();
//    m_udpSendSocket->writeDatagram(udpBuff, QHostAddress::Broadcast, 45454);
}
void BackEnd::broadcastData()
{
    QString buff="Dat";
    buff.append(QDateTime::currentDateTime().toString("dd/MM/yyyy"));
    buff.append("Tim");
    buff.append(QDateTime::currentDateTime().toString("hh:mm:ss:zzz"));
    buff.append(QString("Lat%1Lon%2Cad%3Spe%4Set%5Thr%6Cur%7Tem%8XYZ\n")
                .arg(m_currentData.latitude,0,'f',15)
                .arg(m_currentData.longitude,0,'f',15)
                .arg(m_currentData.cadence)
                .arg(m_currentData.speed)
                .arg(m_currentData.setpoint)
                .arg(m_currentData.throttle)
                .arg(m_currentData.current)
                .arg(m_currentData.temperature)
                );
//    const char* p =buff.toStdString().c_str();
//    m_file->write(qPrintable(buff));
//    m_file->write(p);
    buff.prepend("Vehicle1:");
    QByteArray udpBuff = buff.toUtf8();
    m_udpSendSocket->writeDatagram(udpBuff, QHostAddress::Broadcast, 45454);
}
main_data BackEnd::getCurrentData()
{
    return m_currentData;
}
int BackEnd::getSpeed()
{
    return m_currentData.speed;
}
int BackEnd::getCadence()
{
    return m_currentData.cadence;
}
void BackEnd::keepScreenOn(bool on)
{
#ifdef Q_OS_ANDROID  //only compile this if compiling this for Android
    QtAndroid::runOnAndroidThread([on]{
      QAndroidJniObject activity = QtAndroid::androidActivity();
      if (activity.isValid()) {
        QAndroidJniObject window =
            activity.callObjectMethod("getWindow", "()Landroid/view/Window;");

        if (window.isValid()) {
          const int FLAG_KEEP_SCREEN_ON = 128;
          if (on) {
            window.callMethod<void>("addFlags", "(I)V", FLAG_KEEP_SCREEN_ON);
          } else {
            window.callMethod<void>("clearFlags", "(I)V", FLAG_KEEP_SCREEN_ON);
          }
        }
      }
      QAndroidJniEnvironment env;
      if (env->ExceptionCheck()) {
        env->ExceptionClear();
      }
    });
#endif  //Q_OS_Android
}
void BackEnd::handlePitCall()
{
#ifdef Q_OS_ANDROID  //only compile this if compiling this for Android
//    QString state = myPhone.status();
    myPhone.directCall(m_pitPhoneNumber);  //just make call for now...
    //if not in call, make call
    //else hang up
#endif  //Q_OS_Android
}
void BackEnd::NetworkCheck()
{   //activated every 10 seconds...
    if(!m_tcp_check_sent)
    {  //send a check connection message
        m_tcpSocket->write("check...");
        m_tcp_check_sent=true;
        return;
    }
    //TcpConnect();
}
void BackEnd::resync()
{
    TcpConnect();
}
void BackEnd::TcpConnect()
{
    QString hostIP=m_serverIP; //"10.147.20.148";
    m_tcpSocket->disconnectFromHost();
    m_tcpSocket->connectToHost(hostIP,m_port);
   // connect (m_tcpSocket,SIGNAL(disconnected()),this,SLOT(TcpDisconnect()));
}
void BackEnd::TcpDisconnect()
{
    m_tcpSocket->disconnectFromHost();
}
void BackEnd::TcpSend()
{
    if (!m_tcpSocket->isValid())
    {
        //attempt reconnect!!!
        TcpConnect();
        return;
    }
    QString buff="Dat";
    buff.append(QDateTime::currentDateTime().toString("dd/MM/yyyy"));
    buff.append("Tim");
    buff.append(QDateTime::currentDateTime().toString("hh:mm:ss:zzz"));
    buff.append(QString("Lat%1Lon%2Cad%3Spe%4Set%5Thr%6Cur%7Tem%8XYZ\n")
                .arg(m_currentData.latitude,0,'f',15)
                .arg(m_currentData.longitude,0,'f',15)
                .arg(m_currentData.cadence)
                .arg(m_currentData.speed)
                .arg(m_currentData.setpoint)
                .arg(m_currentData.throttle)
                .arg(m_currentData.current)
                .arg(m_currentData.temperature)
                );
//    const char* p =buff.toStdString().c_str();
//    m_file->write(qPrintable(buff));
//    m_file->write(p);
    buff.prepend("Vehicle1:");
    QByteArray tcpBuff = buff.toUtf8();
    m_tcpSocket->write(tcpBuff);
    m_tcpSocket->flush();
}
void BackEnd::TcpRead()
{
    m_in.startTransaction();
    QString thisComms;
    m_in>>thisComms;
    //do something with the sent communication packet...
    m_tcp_check_sent=false;
    if (!m_in.commitTransaction())
        return;
}
void BackEnd::TcpSessionOpened()
{
    // Save the used configuration
    QNetworkConfiguration config = m_networkSession->configuration();
    QString id;
    if (config.type() == QNetworkConfiguration::UserChoice)
        id = m_networkSession->sessionProperty(QLatin1String("UserChoiceConfiguration")).toString();
    else
        id = config.identifier();
    QSettings settings(QSettings::UserScope, QLatin1String("QtProject"));
    settings.beginGroup(QLatin1String("QtNetwork"));
    settings.setValue(QLatin1String("DefaultNetworkConfiguration"), id);
    settings.endGroup();
}
void BackEnd::TcpError(QAbstractSocket::SocketError socketError)
{
    switch (socketError)
    {
        case QAbstractSocket::RemoteHostClosedError:
        break;
        case QAbstractSocket::HostNotFoundError:
            //check host IP and port settings
        break;
        case QAbstractSocket::ConnectionRefusedError:
            //check host ip, port and pit software running...
        break;
        default:
            //.arg(m_tcpSocket->errorString());
        break;
    }
}

// network sending example following...
//#include <QtWidgets>
//#include <QtNetwork>
//#include <QtCore>

//#include "sender.h"

//Sender::Sender(QWidget *parent)
//    : QWidget(parent)
//{
//    statusLabel = new QLabel(tr("Ready to broadcast datagrams on port 45454"));
//    statusLabel->setWordWrap(true);

//    startButton = new QPushButton(tr("&Start"));
//    auto quitButton = new QPushButton(tr("&Quit"));

//    auto buttonBox = new QDialogButtonBox;
//    buttonBox->addButton(startButton, QDialogButtonBox::ActionRole);
//    buttonBox->addButton(quitButton, QDialogButtonBox::RejectRole);

////! [0]
//    udpSocket = new QUdpSocket(this);
////! [0]

//    connect(startButton, &QPushButton::clicked, this, &Sender::startBroadcasting);
//    connect(quitButton, &QPushButton::clicked, this, &Sender::close);
//    connect(&timer, &QTimer::timeout, this, &Sender::broadcastDatagram);

//    auto mainLayout = new QVBoxLayout;
//    mainLayout->addWidget(statusLabel);
//    mainLayout->addWidget(buttonBox);
//    setLayout(mainLayout);

//    setWindowTitle(tr("Broadcast Sender"));
//}

//void Sender::startBroadcasting()
//{
//    startButton->setEnabled(false);
//    timer.start(1000);
//}

//void Sender::broadcastDatagram()
//{
//    statusLabel->setText(tr("Now broadcasting datagram %1").arg(messageNo));
////! [1]
//    QByteArray datagram = "Broadcast message " + QByteArray::number(messageNo);
//    udpSocket->writeDatagram(datagram, QHostAddress::Broadcast, 45454);
////! [1]
//    ++messageNo;
//}
