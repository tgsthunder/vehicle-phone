#ifndef BACKEND_H
#define BACKEND_H

#include <QObject>
#include <QString>
#include <QtPositioning/QGeoPositionInfoSource>
#include <QtPositioning/QGeoPositionInfo>
#include <QtPositioning/QGeoCoordinate>
#include "BluetoothLE/bluetoothleuart.h"
#ifdef Q_OS_ANDROID
    #include <QtAndroid>
    #include <QAndroidJniEnvironment>
    #include <QUrl>
    #include <QDesktopServices>
#endif //Q_OS_ANDROID
#include <QStandardPaths>
#include <QtGlobal>
#include <QFile>
#include <QDir>
#include <QSettings>
#include <QDataStream>
#include <QTcpSocket>
#include "telephony.h"
#include <QTimer>


QT_BEGIN_NAMESPACE
class QUdpSocket;
class QTcpSocket;
class QNetworkSession;
QT_END_NAMESPACE

class main_data
{
public:
    QDateTime timestamp;
    double latitude;
    double longitude;
    int cadence;
    int speed;
    int setpoint;
    int throttle;
    int current;
    int temperature;
    bool voice;
};

class BackEnd : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString userName READ userName WRITE setUserName NOTIFY userNameChanged)
    Q_PROPERTY(QStringList BTDeviceList READ BTDeviceList NOTIFY btDeviceListChanged)
    Q_PROPERTY(QGeoCoordinate coordinates READ getCurrentLocation NOTIFY locationChanged)
    Q_PROPERTY(QString status READ getStatus NOTIFY statusChanged)
    Q_PROPERTY(int btIndex READ btIndex WRITE setbtIndex)
    Q_PROPERTY(main_data currentData READ getCurrentData NOTIFY mainDataChanged)
    Q_PROPERTY(int speed READ getSpeed)
    Q_PROPERTY(int cadence READ getCadence)
    Q_PROPERTY(QString pitNumber READ getPitNumber WRITE setPitNumber)
    Q_PROPERTY(QString serverIP READ getServerIP WRITE setServerIP)
    Q_PROPERTY(QString serverPort READ getServerPort WRITE setServerPort)
public:
    explicit BackEnd(QObject *parent = nullptr);
    ~BackEnd();
    QStringList BTDeviceList();
    QString userName();
    void setUserName(const QString &userName);
    int btIndex();
    void setbtIndex(int val);
    Q_INVOKABLE void ScanButtonClick();
    Q_INVOKABLE void ConnectButtonClick();
    QGeoCoordinate getCurrentLocation();
    QString getStatus();
    main_data getCurrentData();
    int getSpeed();
    int getCadence();
    QString getServerIP();
    void setServerIP(QString IP);
    QString getPitNumber();
    void setPitNumber(QString number);
    void setServerPort(QString port);
    QString getServerPort();
    Telephony myPhone;
public slots:
    void positionUpdated(const QGeoPositionInfo &info);
    void resync();
private slots:
    void DataHandler(const QString &s);
//    void connectDevice();
//    void sendData();
    void changedState(bluetoothleUART::bluetoothleState state);
    void handlePitCall();
    void TcpSend();
    void TcpRead();
    void TcpSessionOpened();
    void TcpDisconnect();
    void TcpError(QAbstractSocket::SocketError socketError);
    void NetworkCheck();
signals:
    void userNameChanged();
    void btDeviceListChanged();
    void locationChanged();
    void statusChanged();
    void btScan();
    void connectToDevice(int i);
    void mainDataChanged();
    void togglePhone();
private:
    QString m_userName;
    QStringList m_BTDeviceList;
    QString m_status;
    QGeoCoordinate m_currentLocation;
    QGeoPositionInfo m_GeoInfo;
    QGeoPositionInfoSource *m_GeoSource;
    bluetoothleUART m_bleConnection;
    int m_btIndex;
    void changeStatus(QString newStatus);
    main_data m_currentData;
    QString m_BlueBuffer;
    void keepScreenOn(bool on);
    QFile* m_file;
    QDir* m_dir;
    void writeToFile();
    void broadcastData();
    void TcpConnect();
    int myStrToI(QString val);
    QUdpSocket *m_udpSendSocket = nullptr;
    QTcpSocket *m_tcpSocket=nullptr;
    QDataStream m_in;
    QNetworkSession *m_networkSession=nullptr;
    QString m_serverIP;
    int m_port;
    QString m_pitPhoneNumber;
    QSettings* m_settings;
    bool m_attempt_reconnect=false;
    bool m_bluetooth_initialised=false;
    bool m_tcp_check_sent;
    int m_reconnect_count=0;
    int m_displayCounter=0;
    QTimer *m_networkCheck;
};
#endif // BACKEND_H
