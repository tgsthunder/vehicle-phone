#include "telephony.h"
#include <QAndroidJniEnvironment>

/*  from JAVA code to C++ :https://stackoverflow.com/questions/29317048/how-to-make-a-call-with-qt-directly-from-the-application
Intent callIntent = new callIntent(Intent.ACTION_CALL);
callIntent.setPackage("com.android.phone");          // force native dialer  (Android < 5)
callIntent.setPackage("com.android.server.telecom"); // force native dialer  (Android >= 5)
callIntent.setData(Uri.parse("tel:" + number));
callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
startActivity(callIntent);
*/
void Telephony::directCall(QString number)
{
#if defined(Q_OS_ANDROID)
    // get the Qt android activity
    QAndroidJniObject activity =  QAndroidJniObject::callStaticObjectMethod("org/qtproject/qt5/android/QtNative", "activity", "()Landroid/app/Activity;");
    //
    if (activity.isValid())
    {
    // real Java code to C++ code
    // Intent callIntent = new callIntent(Intent.ACTION_CALL);
    QAndroidJniObject callConstant = QAndroidJniObject::getStaticObjectField<jstring>("android/content/Intent", "ACTION_CALL");
    QAndroidJniObject callIntent("android/content/Intent",  "(Ljava/lang/String;)V", callConstant.object());
    // callIntent.setPackage("com.android.phone"); (<= 4.4w)  intent.setPackage("com.android.server.telecom");  (>= 5)
    QAndroidJniObject package;
    if(QtAndroid::androidSdkVersion() >= 21)
        package = QAndroidJniObject::fromString("com.android.server.telecom");
    else
        package = QAndroidJniObject::fromString("com.android.phone");
    callIntent.callObjectMethod("setPackage", "(Ljava/lang/String;)Landroid/content/Intent;", package.object<jstring>());
    // callIntent.setData(Uri.parse("tel:" + number));
    QAndroidJniObject jNumber = QAndroidJniObject::fromString(QString("tel:%1").arg(number));
    QAndroidJniObject uri = QAndroidJniObject::callStaticObjectMethod("android/net/Uri","parse","(Ljava/lang/String;)Landroid/net/Uri;", jNumber.object());
    callIntent.callObjectMethod("setData", "(Landroid/net/Uri;)Landroid/content/Intent;", uri.object<jobject>());
    // callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    jint flag = QAndroidJniObject::getStaticField<jint>("android/content/Intent", "FLAG_ACTIVITY_NEW_TASK");
    callIntent.callObjectMethod("setFlags", "(I)Landroid/content/Intent;", flag);
    //startActivity(callIntent);
    activity.callMethod<void>("startActivity","(Landroid/content/Intent;)V", callIntent.object<jobject>());
    }
    else
        qDebug() << "Something wrong with Qt activity...";
#else
    qDebug() << "Does nothing here...";
#endif
}

QString Telephony::status()
{  //check https://stackoverflow.com/questions/30108026/calling-external-activity-by-explicit-intent-from-qt-app-on-android-putextra
    //check https://forum.qt.io/topic/73662/imei-on-android-with-jna
#if defined(Q_OS_ANDROID)

//    int result = QAndroidJniObject::callStaticMethod<jint>
//            ("vehicle/telephony" // java class name
//            , "PhoneStateReceiver" // method name
//            , "(I)I" // signature
//            , "");
//    qDebug() << result;
//    return "OK";


    // get the Qt android activity
//    QAndroidJniObject activity =  QAndroidJniObject::callStaticObjectMethod("org/qtproject/qt5/android/QtNative", "activity", "()Landroid/app/Activity;");
//    //
//    if (activity.isValid()){
//        // real Java code to C++ code
//        // Intent callIntent = new callIntent(Intent.ACTION_CALL);
//        QAndroidJniObject callConstant = QAndroidJniObject::getStaticObjectField<jstring>("android/content/Intent", "PHONE_STATE");
//        QAndroidJniObject callIntent("android/content/Intent",  "(Ljava/lang/String;)V", callConstant.object());
//        // callIntent.setPackage("com.android.phone"); (<= 4.4w)  intent.setPackage("com.android.server.telecom");  (>= 5)
//        QAndroidJniObject package;
//        if(QtAndroid::androidSdkVersion() >= 21)
//            package = QAndroidJniObject::fromString("com.android.server.telecom");
//        else
//            package = QAndroidJniObject::fromString("com.android.phone");
////        QAndroidJniObject EXTRA_STATE = QAndroidJniObject::getStaticObjectField("android/provider/TelephonyManager",
////                                                                                          "EXTRA_STATE");
//        auto EXTRA_STATE = QAndroidJniObject::getStaticObjectField("android/content/Intent", "EXTRA_STATE", "Ljava/lang/String;");
//        TelephonyManager myTM;
//    }
//==============================================================================================================
//    QAndroidJniEnvironment env;
//    jclass contextClass = env->FindClass("android/content/Context");
//    jfieldID fieldId = env->GetStaticFieldID(contextClass, "TELEPHONY_SERVICE", "Ljava/lang/String;");
//    jstring telephonyManagerType = (jstring) env->GetStaticObjectField(contextClass, fieldId);

//    jclass telephonyManagerClass = env->FindClass("android/telephony/TelephonyManager");
//    jmethodID methodId = env->GetMethodID(contextClass, "getSystemService", "(Ljava/lang/String;)Ljava/lang/Object;");

//    QAndroidJniObject qtActivityObj = QAndroidJniObject::callStaticObjectMethod("org/qtproject/qt5/android/QtNative",  "activity", "()Landroid/app/Activity;");
//    jobject telephonyManager = env->CallObjectMethod(qtActivityObj.object<jobject>(), methodId, telephonyManagerType);
////****************************** bugger!!!!
//    methodId = env->GetMethodID(telephonyManagerClass, "getCallState", "(Ljava/lang/String);Ljava/lang/Object;");
//    jstring jstr = (jstring) env->CallObjectMethod(telephonyManager, methodId);
//    jsize len = env->GetStringUTFLength(jstr);
//    char* buf_extra = new char[32];
//    env->GetStringUTFRegion(jstr, 0, len, buf_extra);
//    QString state(buf_extra);
//    delete buf_extra;
//    //data is in state...
//    qDebug() << state;
//    //0 = idle
//    //1 = ringing
//    //2 = off hook
//    return state;
/*
public class PhoneStateReceiver extends BroadcastReceiver {
    public static String TAG="PhoneStateReceiver";
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("android.intent.action.PHONE_STATE")) { 
            String state = intent.getStringExtra(TelephonyManager.EXTRA_STATE);
            Log.d(TAG,"PhoneStateReceiver**Call State=" + state);
            if (state.equals(TelephonyManager.EXTRA_STATE_IDLE)) {
                //idle
            } else if (state.equals(TelephonyManager.EXTRA_STATE_RINGING)) { 
                // Incoming call
                String incomingNumber = 
                        intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER);
                //Incoming call + incomingNumber)
                if (!killCall(context)) { // Using the method defined earlier
                    //can't kill call!!!
                }
            } else if (state.equals(TelephonyManager.EXTRA_STATE_OFFHOOK)) {
                //off hook
            }
        } else if (intent.getAction().equals("android.intent.action.NEW_OUTGOING_CALL")) { 
            // Outgoing call
            String outgoingNumber = intent.getStringExtra(Intent.EXTRA_PHONE_NUMBER);
            //Outgoing call + outgoingNumber
            setResultData(null); // Kills the outgoing call
        } else {
            //unexpected status
        }
    }
}
*/
#else
    qDebug() << "Does nothing here...";
    return "nothing here...";
#endif
}

void Telephony::hangUp()
{
/*
    public boolean killCall(Context context) {
        try {
            // Get the boring old TelephonyManager
            TelephonyManager telephonyManager =
                    (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

            // Get the getITelephony() method
            Class classTelephony = Class.forName(telephonyManager.getClass().getName());
            Method methodGetITelephony = classTelephony.getDeclaredMethod("getITelephony");

            // Ignore that the method is supposed to be private
            methodGetITelephony.setAccessible(true);

            // Invoke getITelephony() to get the ITelephony interface
            Object telephonyInterface = methodGetITelephony.invoke(telephonyManager);

            // Get the endCall method from ITelephony
            Class telephonyInterfaceClass =  
                    Class.forName(telephonyInterface.getClass().getName());
            Method methodEndCall = telephonyInterfaceClass.getDeclaredMethod("endCall");

            // Invoke endCall()
            methodEndCall.invoke(telephonyInterface);

        } catch (Exception ex) { // Many things can go wrong with reflection calls
            Log.d(TAG,"PhoneStateReceiver **" + ex.toString());
            return false;
        }
        return true;
    }
*/    
}

QString Telephony::getIMEI()
{  //cdg tested and works...
    QAndroidJniEnvironment env;
    jclass contextClass = env->FindClass("android/content/Context");
    jfieldID fieldId = env->GetStaticFieldID(contextClass, "TELEPHONY_SERVICE", "Ljava/lang/String;");
    jstring telephonyManagerType = (jstring) env->GetStaticObjectField(contextClass, fieldId);

    jclass telephonyManagerClass = env->FindClass("android/telephony/TelephonyManager");
    jmethodID methodId = env->GetMethodID(contextClass, "getSystemService", "(Ljava/lang/String;)Ljava/lang/Object;");

   QAndroidJniObject qtActivityObj = QAndroidJniObject::callStaticObjectMethod("org/qtproject/qt5/android/QtNative",  "activity", "()Landroid/app/Activity;");
    jobject telephonyManager = env->CallObjectMethod(qtActivityObj.object<jobject>(), methodId, telephonyManagerType);

    methodId = env->GetMethodID(telephonyManagerClass, "getDeviceId", "()Ljava/lang/String;");
    jstring jstr = (jstring) env->CallObjectMethod(telephonyManager, methodId);

    jsize len = env->GetStringUTFLength(jstr);
    char* buf_devid = new char[32];
    env->GetStringUTFRegion(jstr, 0, len, buf_devid);
    QString imei(buf_devid);
    qDebug() << imei;
    delete buf_devid;
    return imei;
}
