#ifndef TELEPHONY_H
#define TELEPHONY_H
#include <QObject>
#include <QString>
#include <QDebug>
#if defined(Q_OS_ANDROID)
#include <QtAndroid>
#include <QAndroidJniObject>
#endif
#include <QDesktopServices>
#include <QUrl>
//from https://stackoverflow.com/questions/29317048/how-to-make-a-call-with-qt-directly-from-the-application
class Telephony: public QObject
{
	Q_OBJECT
    Q_PROPERTY(QString status READ status NOTIFY statusChanged)
public:
	Q_INVOKABLE void directCall(QString number);
	QString status();
    Q_INVOKABLE void hangUp();
    QString getIMEI();
signals:
    void statusChanged();
};

#endif // TELEPHONY_H
