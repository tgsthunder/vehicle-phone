import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Controls.Styles 1.4
import QtQuick.Extras 1.4
import io.qt.examples.backend 1.0

//import QtQuick.Window 2.1

ApplicationWindow {
    id: root
    visible: true
//    height: screen.width
//    width: screen.height
    property var gaugeScaling: 0.95
    width: 640
    height: 480
    title: qsTr("Tabs")
    BackEnd { //set up the links to the backend
        id: backend
        onBtDeviceListChanged: {cb_bluetooth.update()
            cb_bluetooth.enabled=true
            btConnect.enabled=true
        }
        onLocationChanged: {
            latitude.text=backend.coordinates.latitude
            longitude.text=backend.coordinates.longitude
            latitude.color="green"
            longitude.color="green"
        }
        onStatusChanged: {statusLabel.text=backend.status}
        onMainDataChanged: {speedometer.value=backend.speed
            tachometer.value= backend.cadence
        }
    }
    SwipeView {
        id: swipeView
        anchors.fill: parent
        currentIndex: 0
        Item {
            id: page1
            DelayButton {
                id: bexit
                text: "Exit"
                anchors.top: parent.top
                anchors.topMargin: 10
                anchors.right: parent.right
                anchors.rightMargin: 10
                delay: 1500
                onActivated: Qt.quit()
            }
            DelayButton{
                id: bresync
                text: "Re-Sync"
                anchors.top: parent.top
                anchors.topMargin: 10
                anchors.left: parent.left
                anchors.leftMargin: 10
                delay: 500
                onActivated: backend.resync();
            }

            Button {
                id: btScan
                text: "BT Scan"
                anchors.top: parent.top
                anchors.topMargin: 10
                anchors.horizontalCenter: parent.horizontalCenter
                onClicked: backend.ScanButtonClick();
            }
            ComboBox {
                id: cb_bluetooth
                currentIndex: 0
                model: backend.BTDeviceList
                anchors.top: btScan.bottom
                anchors.topMargin: 20
                anchors.horizontalCenter: parent.horizontalCenter
                width: 200
                enabled: false  //enabled if there are devices to select
                onActivated: {backend.btIndex = cb_bluetooth.currentIndex} //backend.BTDeviceList(currentIndex).text;
            }
            Label{
                text: "Discovered Devices"
                anchors.bottom: cb_bluetooth.top
                anchors.horizontalCenter: cb_bluetooth.horizontalCenter
            }
            Button {
                id: btConnect
                text: "BT Connect"
                anchors.top: cb_bluetooth.bottom
                anchors.topMargin: 10
                anchors.horizontalCenter: parent.horizontalCenter
                onClicked: backend.ConnectButtonClick();
                enabled: false  //will be enabled when there are BT devices in the combobox
            }
//            Label{
//                id: phoneLabel
//                text: "Pit Phone"
//                anchors.top: btConnect.bottom
//                anchors.horizontalCenter: btConnect.horizontalCenter
//                anchors.topMargin: 10
//            }
//            TextField{
//                id: pitPhone
//                text: backend.pitNumber
////                placeholderText: qsTr("User name")
//                width: 120
//                anchors.top: phoneLabel.bottom
//                anchors.horizontalCenter: phoneLabel.horizontalCenter
//                anchors.topMargin: 10
//                onTextChanged: backend.pitNumber = text
//            }
            Label{
                id: serverIPLabel
                text: "Server IP"
                anchors.top: btConnect.bottom
                anchors.horizontalCenter: btConnect.horizontalCenter
                anchors.topMargin: 10
            }
            TextField{
                id: serverIP
                text: backend.serverIP
//                placeholderText: qsTr("User name")
                width: 120
                anchors.top: serverIPLabel.bottom
                anchors.horizontalCenter: serverIPLabel.horizontalCenter
                anchors.topMargin: 10
                onTextChanged: backend.serverIP = text
            }
            Label{
                id: serverPortLabel
                text: "Server Port"
                anchors.top: bexit.bottom
                anchors.right: bexit.right
                anchors.topMargin: 10
            }
            TextField{
                id: serverPort
                text: backend.serverPort
                    //"5100"
                width: 120
                anchors.top: serverPortLabel.bottom
                anchors.right: bexit.right
                anchors.topMargin: 10
                onTextChanged: backend.serverPort = text;
            }
            Label{
                id: statusLabel
                text: "idle"
                color: "gray"
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 10
                anchors.horizontalCenter: parent.horizontalCenter
            }
            Label{  //GPS Latitude display
                id: latitude
                text: "NAN"
                color: "red"   //Qt.rgba(1, 1, 1, 0.13)
                anchors.left: parent.left
                anchors.leftMargin: 10
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 30
            }
            Label{  //GPS Longitude display
                id: longitude
                text: "NAN"
                color: "red"
                anchors.left: parent.left
                anchors.leftMargin: 10
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 10
            }
        }
        Item {
            id: page2
            Item {
                id: container
                width: root.width
                height: Math.min(root.width, root.height)
                anchors.centerIn: parent
                Row {
                    id: gaugeRow
                    spacing: container.width * 0.02
                    anchors.centerIn: parent
//                    Item {
//                        width: height
//                        height: container.height * 0.25 - gaugeRow.spacing
//                        anchors.verticalCenter: parent.verticalCenter
//                    }
                    CircularGauge {
                        id: speedometer
                        value: 40
                        anchors.verticalCenter: parent.verticalCenter
                        maximumValue: 70
                        // We set the width to the height, because the height will always be
                        // the more limited factor. Also, all circular controls letterbox
                        // their contents to ensure that they remain circular. However, we
                        // don't want to extra space on the left and right of our gauges,
                        // because they're laid out horizontally, and that would create
                        // large horizontal gaps between gauges on wide screens.
                        width: height
                        height: container.height * gaugeScaling
                        Behavior on value {SpringAnimation{spring: 2; damping: 0.2}}
                        style: DashboardGaugeStyle {}
                    }
                    CircularGauge {
                        id: tachometer
                        width: height
                        height: container.height * 0.5 - gaugeRow.spacing
                        value: 9  //valueSource.rpm
                        maximumValue: 120
                        anchors.verticalCenter: parent.verticalCenter
                        Behavior on value {SpringAnimation{spring: 2; damping: 0.2}}
                        style: TachometerStyle {}
                    }
                }
            }
        }
//        Item{
//            id: page3
//        }
    }
    PageIndicator {
        id: indicator
        count: swipeView.count
        currentIndex: swipeView.currentIndex
        anchors.bottom: swipeView.bottom
        anchors.horizontalCenter: parent.horizontalCenter
    }
}
